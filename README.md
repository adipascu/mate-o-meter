# MATE-o-Meter

## Description

How much Club Mate do you currently have in your bottle?

[Graph with the sound processing pipeline](https://excalidraw.com/#json=GAldOj-_Dh8ch3P-dn_cn,Ui_eaA6B7ecm-z6NAz-hQQ)

Inspired by: https://github.com/tuxcodejohn/mateometer/blob/2ae63f02a500afd6155389f13128a39345f5352f/mateformeln.py#L21