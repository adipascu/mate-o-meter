const RMS_CONSTANT = 1 / Math.sqrt(2);

export default (
  audioContext: AudioContext,
  bufferLength: number,
  minFrequency: number,
  maxFrequency: number,
) => {
  const { sampleRate } = audioContext;
  const minIndex = Math.floor(minFrequency / (sampleRate / (2 * bufferLength)));
  const maxIndex = Math.min(
    Math.floor(maxFrequency / (sampleRate / (2 * bufferLength))),
    bufferLength - 1,
  );

  return (data: Uint8Array) => {
    let maxAmplitude = -Infinity;
    let dominantFrequency = -1;

    for (let i = minIndex; i <= maxIndex; i += 1) {
      if (data[i] > maxAmplitude) {
        maxAmplitude = data[i];
        dominantFrequency = i * (sampleRate / (2 * bufferLength));
      }
    }

    return {
      dominantFrequency,
      signalRms: maxAmplitude * RMS_CONSTANT,
    };
  };
};
