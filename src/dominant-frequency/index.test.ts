import data from "./155.json";
import createFrequencyAnalyzer from ".";

const FREQ_EPSILON = 0;
describe("getDominantFrequency", () => {
  it("should return a value close to the expected dominant frequency", () => {
    const getDominantFrequency = createFrequencyAnalyzer(
      {
        sampleRate: 44100,
      } as any,
      data.length,
      0,
      20000,
    );
    const { dominantFrequency } = getDominantFrequency(new Uint8Array(data));

    console.warn({ dominantFrequency });
    expect(dominantFrequency).toBeCloseTo(155, FREQ_EPSILON);
  });
});
