export default (canvas: HTMLCanvasElement) => {
  const context = canvas.getContext("2d");
  if (!context) {
    throw new Error("Could not get canvas context");
  }
  context.lineWidth = 1;
  context.strokeStyle = "rgb(0, 255, 0)";
  context.fillStyle = "rgb(0, 0, 0)";
  return (data: Uint8Array) => {
    context.fillRect(0, 0, canvas.width, canvas.height);
    context.beginPath();
    const sliceWidth = canvas.width / data.length;
    for (let i = 0, x = 0; i < data.length; i += 1, x += sliceWidth) {
      const v = data[i] / 128.0;
      const y = (v * canvas.height) / 2;
      if (i === 0) {
        context.moveTo(x, canvas.height - y);
      } else {
        context.lineTo(x, canvas.height - y);
      }
    }
    context.stroke();
  };
};
