import { createEffect } from "solid-js";
import { Layout, PlotData, react as updatePlotly } from "plotly.js-dist-min";

const Plot = (props: { data: Uint8Array }) => {
  // eslint-disable-next-line no-type-assertion/no-type-assertion
  const container = (
    <div
      style={{
        width: "1200px",
        height: "300px",
      }}
    ></div>
  ) as HTMLDivElement;

  createEffect(() => {
    const trace1: Partial<PlotData> = {
      x: [...props.data].map(
        (_, idx) => (idx * 44100) / (2 * props.data.length),
      ),
      y: [...props.data],
      type: "scatter",
    };

    const data2 = [trace1];

    const layout: Partial<Layout> = {
      xaxis: {
        type: "log",
        autorange: true,
      },
      yaxis: {
        autorange: true,
      },
    };

    updatePlotly(container, data2, layout);
  });

  return container;
};

export default Plot;
