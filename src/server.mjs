import {
  mediaDevices,
  AudioContext,
  MediaStreamAudioSourceNode,
  GainNode,
} from "node-web-audio-api";

console.log(
  "MediaDevices::getUserMedia - mic feedback, be careful with volume...)"
);

const mediaStream = await mediaDevices.getUserMedia({ audio: true });

const audioContext = new AudioContext();
await audioContext.resume();

const source = audioContext.createMediaStreamSource(mediaStream);

var filter = audioContext.createBiquadFilter();
filter.type = "highpass";
filter.frequency.value = 15000;
filter.Q.value = 10;

source.connect(filter);
var analyserFilter = audioContext.createAnalyser();
filter.connect(analyserFilter);

const data = new Uint8Array(analyserFilter.frequencyBinCount);
setInterval(() => {
  console.clear();
  analyserFilter.getByteFrequencyData(data);
  const max = Math.max(...data);
  const maxIndex = data.indexOf(max);
  const maxFreq = maxIndex * (audioContext.sampleRate / analyserFilter.fftSize);
  console.log(maxFreq.toFixed(2) + "Hz");
}, 1000 / 60);
