import {
  Component,
  createEffect,
  createSignal,
  onCleanup,
  untrack,
} from "solid-js";
import createFrequencyAnalyzer from "./dominant-frequency";
import plotAnalyzer from "./plot-nalyzer";
import loadAudio from "./load-audio";
import mateFormula from "./mate-formula";
import Plot from "./plot";

await new Promise<void>((resolve) => {
  const onClick = () => {
    document.removeEventListener("click", onClick);
    resolve();
  };
  document.addEventListener("click", onClick);
});

// const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
const audioContext = new AudioContext();
// var source = audioContext.createMediaStreamSource(stream);

const source = await loadAudio("/src/empty-mate.m4a", audioContext);

// source.connect(audioContext.destination);
// const source = await loadAudio("/src/low-mate.m4a", audioContext);
// source.connect(audioContext.destination);

const analyzer = audioContext.createAnalyser();

const [fftSize, setFftSize] = createSignal(13);
let rawAnalyzerBuffer = new Uint8Array(analyzer.frequencyBinCount);

createEffect(() => {
  analyzer.fftSize = 2 ** fftSize();
  rawAnalyzerBuffer = new Uint8Array(analyzer.frequencyBinCount);
});
const [smoothingTimeConstant, setSmoothingTimeConstant] = createSignal(
  analyzer.smoothingTimeConstant
);
createEffect(() => {
  analyzer.smoothingTimeConstant = smoothingTimeConstant();
});
// analyzer.fftSize = 2 ** 15;
// analyzer.smoothingTimeConstant = 0;

source.connect(analyzer);

const MIN_FREQUENCY = 156;
const MAX_FREQUENCY = 1118;

const getDominantFrequency = createFrequencyAnalyzer(
  audioContext,
  rawAnalyzerBuffer.length,
  MIN_FREQUENCY,
  MAX_FREQUENCY
);

const App: Component = () => {
  const [realTimeBuffer, setRealTimeBuffer] = createSignal(
    new Uint8Array(rawAnalyzerBuffer.length)
  );

  const [snapshotBuffer, setSnapshotBuffer] = createSignal(
    new Uint8Array(rawAnalyzerBuffer.length)
  );

  createEffect(() => {
    const interval = setInterval(() => {
      const buffer = new Uint8Array(rawAnalyzerBuffer.length);
      analyzer.getByteFrequencyData(buffer);
      setRealTimeBuffer(buffer);
      const { dominantFrequency, signalRms } = getDominantFrequency(buffer);

      // const mateLevel = mateFormula(dominantFrequency);
      // console.log({ dominantFrequency, mateLevel });
    }, 1000 / 60);

    onCleanup(() => {
      clearInterval(interval);
    });
  });
  // const draw2 = plotAnalyzer(canvas2);

  return (
    <div style={{ display: "flex", "flex-direction": "column" }}>
      <button
        onClick={() => {
          // copy rawAnalyzerBuffer to snapshot variable
          (window as any).snapshot = realTimeBuffer;
          setSnapshotBuffer(realTimeBuffer);
          // draw2((window as any).snapshot);
          const { dominantFrequency, signalRms } =
            getDominantFrequency(rawAnalyzerBuffer);
          const mateLevel = mateFormula(dominantFrequency);
          console.log({ dominantFrequency, mateLevel });
        }}
      >
        Grab snapshot
      </button>
      <Plot data={realTimeBuffer()} />
      <input
        type="range"
        value={untrack(fftSize)}
        onInput={(event) => {
          setFftSize(Number((event.target as HTMLInputElement).value));
        }}
        min={5}
        max={15}
      />
      <input
        type="range"
        value={untrack(smoothingTimeConstant)}
        onInput={(event) => {
          setSmoothingTimeConstant(
            Number((event.target as HTMLInputElement).value)
          );
        }}
        min={0}
        max={1}
        step={0.001}
      />
      <Plot data={snapshotBuffer()} />
    </div>
  );
};

export default App;
