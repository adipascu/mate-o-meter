export default async (url: string, audioContext: AudioContext) => {
  // Fetch the ALAC file and read it as an ArrayBuffer
  const response = await fetch(url);
  const arrayBuffer = await response.arrayBuffer();

  // Decode the ArrayBuffer
  const audioBuffer = await audioContext.decodeAudioData(arrayBuffer);

  // Create an AudioBufferSourceNode and set its buffer property
  const source = audioContext.createBufferSource();
  source.buffer = audioBuffer;

  source.loop = true;
  source.start();
  return source;
};
