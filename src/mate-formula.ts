// taken from https://github.com/tuxcodejohn/mateometer/blob/2ae63f02a500afd6155389f13128a39345f5352f/mateformeln.py#L21

export default (frequency: number) => 1.02 - 25000 / frequency ** 2;
