/* eslint-disable import/no-extraneous-dependencies */
import { defineConfig } from "vite";
import solidPlugin from "vite-plugin-solid";

// eslint-disable-next-line import/no-unused-modules
export default defineConfig({
  plugins: [solidPlugin()],
  server: {
    port: 3000,
  },
  build: {
    target: "esnext",
  },
});
